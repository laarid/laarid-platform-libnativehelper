/* include/ */
#include <nativehelper/JNIHelp.h>
#include <nativehelper/JniInvocation.h>
#include <nativehelper/module_api.h>
#include <nativehelper/ScopedBytes.h>
#include <nativehelper/ScopedLocalFrame.h>
#include <nativehelper/ScopedLocalRef.h>
#include <nativehelper/ScopedPrimitiveArray.h>
#include <nativehelper/ScopedStringChars.h>
#include <nativehelper/ScopedUtfChars.h>
#include <nativehelper/toStringArray.h>

/* include_jni/ */
#include <nativehelper/jni.h>

/* platform_include/ */
#include <nativehelper/detail/signature_checker.h>
#include <nativehelper/detail/signature_checker.h>
#include <nativehelper/jni_macros.h>

/* header_only_include/ */
#include <nativehelper/nativehelper_utils.h>
#include <nativehelper/scoped_bytes.h>
#include <nativehelper/scoped_local_frame.h>
#include <nativehelper/scoped_local_ref.h>
#include <nativehelper/scoped_primitive_array.h>
#include <nativehelper/scoped_string_chars.h>
#include <nativehelper/scoped_utf_chars.h>

int main() { return 0; }
